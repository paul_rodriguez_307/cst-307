#
#ArraysandCacheTiming
#
#
#The purpose of this program is to create 3 arrays that represent
#cache, memory, and virtual memory.
#It then simulates a search for a specific piece of data in
#the cache, then the memory, then the virtual memory.
#
#Worked with Connor, Lamarr, and Alec
#



.data

#create a variable to store the array
#the space is the amount of integers being stored times the size of each int
cacheArr:    .space 20
memArr:      .space 40
virMemArr:   .space 40
nl:          .asciiz    "\n"
prompt:      .asciiz    "Please enter a number to find in the cache: "
prompt2:     .asciiz    "\n You are entering the cache. \n"
prompt3:     .asciiz    "\n You are entering memory. \n"
prompt4:     .asciiz    "\n You are entering virtual memory. \n"
found:       .asciiz    "\n The value was found.\n"
notFound:    .asciiz    "\n The value was not found.\n"
cacheTime:   .asciiz    "\nThis took 20 nanoseconds.\n"
memTime:     .asciiz    "\nThis took 80 nanoseconds.\n"
virMemTime:  .asciiz    "\nThis took 480026 nanoseconds.\n"

.text
main:

#create the timer in register k1
addi $k1, $zero, 0

#prints the prompt
li $v0, 4
la $a0, prompt
syscall

#reads in the user's value
li $v0, 5
syscall

#moving the user input to k0
move $k0, $v0
li $v0, 1
addi $a0, $k0, 0
syscall


#storing for the cacheArr
addi $s0, $zero, 1
addi $s1, $zero, 2
addi $s2, $zero, 3
addi $s3, $zero, 4
addi $s4, $zero, 5
#the index is stored in t0 for the cacheArr
addi $t0, $zero, 0
sw $s0, cacheArr($t0)
    addi $t0, $t0, 4
sw $s1, cacheArr($t0)
    addi $t0, $t0, 4
sw $s2, cacheArr($t0)
    addi $t0, $t0, 4
sw $s3, cacheArr($t0)
    addi $t0, $t0, 4
sw $s4, cacheArr($t0)

#storing for the memArr
addi $s0, $zero, 10
addi $s1, $zero, 11
addi $s2, $zero, 12
addi $s3, $zero, 13
addi $s4, $zero, 14
addi $s5, $zero, 15
addi $s6, $zero, 16
addi $s7, $zero, 17
addi $t8, $zero, 18
addi $t9, $zero, 19
#the index is stored in t0 for the memArr
addi $t1, $zero, 0
sw $s0, memArr($t1)
addi $t1, $t1, 4
sw $s1, memArr($t1)
addi $t1, $t1, 4
sw $s2, memArr($t1)
addi $t1, $t1, 4
sw $s3, memArr($t1)
addi $t1, $t1, 4
sw $s4, memArr($t1)
addi $t1, $t1, 4
sw $s5, memArr($t1)
addi $t1, $t1, 4
sw $s6, memArr($t1)
addi $t1, $t1, 4
sw $s7, memArr($t1)
addi $t1, $t1, 4
sw $t8, memArr($t1)
addi $t1, $t1, 4
sw $t9, memArr($t1)

#storing for the virMemArr
addi $s0, $zero, 20
addi $s1, $zero, 21
addi $s2, $zero, 22
addi $s3, $zero, 23
addi $s4, $zero, 24
addi $s5, $zero, 25
addi $s6, $zero, 26
addi $s7, $zero, 27
addi $t8, $zero, 28
addi $t9, $zero, 29
#the index is stored in t0 for the memArr
addi $t2, $zero, 0
sw $s0, virMemArr($t2)
addi $t2, $t2, 4
sw $s1, virMemArr($t2)
addi $t2, $t2, 4
sw $s2, virMemArr($t2)
addi $t2, $t2, 4
sw $s3, virMemArr($t2)
addi $t2, $t2, 4
sw $s4, virMemArr($t2)
addi $t2, $t2, 4
sw $s5, virMemArr($t2)
addi $t2, $t2, 4
sw $s6, virMemArr($t2)
addi $t2, $t2, 4
sw $s7, virMemArr($t2)
addi $t2, $t2, 4
sw $t8, virMemArr($t2)
addi $t2, $t2, 4
sw $t9, virMemArr($t2)

#clear registers for the cache loop
addi $t6, $zero, 0
addi $t7, $zero, 0

#clear registers for the memory loop
addi $s0, $zero, 0
addi $s1, $zero, 0

#clear registers for the virtual memory loop
addi $s2, $zero, 0
addi $s3, $zero, 0

j printcache

#loop through the cache to check for the new word
whileCache:
    #this will exit the loop if the user inputted value is found
    beq $t7, $k0, printfoundcache
    #this will exit the loop if you get to the final element in the list
    beq $t7, 5, printmem

    #load the current word in the list into a new register
    lw $t7, cacheArr($t6)

    #iterate one word up in the list, or in this case 4 bytes further
    addi $t6, $t6, 4

    #prints the current element
    li $v0, 1
    addi $a0, $t7, 0
    syscall

    #prints a new line
    li $v0, 4
    la $a0, nl
    syscall

    j whileCache

#loop through the memory to check for the new word
whileMem:
    #this will exit the loop if the user inputted value is found
    beq $s1, $k0, printfoundmem
    #this will exit the loop if you get to the final element in the list
    beq $s1, 19, printvmem

    #load the current word in the list into a new register
    lw $s1, memArr($s0)

    #iterate one word up in the list, or in this case 4 bytes further
    addi $s0, $s0, 4

    #prints the current element
    li $v0, 1
    addi $a0, $s1, 0
    syscall

    #prints a new line
    li $v0, 4
    la $a0, nl
    syscall

    j whileMem

#loop through the cache to check for the new word
whileVirMem:
    #this will exit the loop if the user inputted value is found
    beq $s3, $k0, printfoundmem
    #this will exit the loop if you get to the final element in the list
    beq $s3, 29, printnotfound

    #load the current word in the list into a new register
    lw $s3, virMemArr($s2)

    #iterate one word up in the list, or in this case 4 bytes further
    addi $s2, $s2, 4

    #prints the current element
    li $v0, 1
    addi $a0, $s3, 0
    syscall

    #prints a new line
    li $v0, 4
    la $a0, nl
    syscall

    j whileVirMem

printcache:

    #prints cache prompt
    li $v0, 4
    la $a0, prompt2
    syscall

    j whileCache

printmem:

    #prints memory prompt
    li $v0, 4
    la $a0, prompt3
    syscall

    #add 1 for navigating to the memory
    addi $k1, $k1, 1

    j whileMem

printvmem:

    #prints virtual memory prompt
    li $v0, 4
    la $a0, prompt3
    syscall

    #add 1 for navigating to the virtual memory
addi $k1, $k1, 1

    j whileVirMem

printfoundcache:

    #prints found prompt
    li $v0, 4
    la $a0, found
    syscall

    j timecheck

printfoundmem:

    #places value inside the first position in the cache
    sw $k0, cacheArr($zero)

    #place the last element of the cache in k0
    #this is so the loop only goes through the cache to display it again
    addi $k0, $zero, 5

    #clear registers for the cache loop
    addi $t6, $zero, 0
    addi $t7, $zero, 0

    j printcache

printnotfound:

    #prints not found prompt
    li $v0, 4
    la $a0, notFound
    syscall

    #places value inside the first position in the cache
    sw $k0, cacheArr($zero)

    #place the last element of the cache in k0
    #this is so the loop only goes through the cache to display it again
    addi $k0, $zero, 5

    #clear registers for the cache loop
    addi $t6, $zero, 0
    addi $t7, $zero, 0

    j whileCache

timecache:

    #prints the time the program took
    li $v0, 4
    la $a0, cacheTime
    syscall

    j exit

timemem:

    #prints the time the program took
    li $v0, 4
    la $a0, memTime
    syscall

    j exit

timevirmem:

    #prints the time the program took
    li $v0, 4
    la $a0,virMemTime
    syscall

    j exit

timecheck:

    #depending on the value in k1, the correct time for the program will be printed
    beq $k1, 0, timecache
    beq $k1, 1, timemem
    beq $k1, 2, timevirmem

exit:

    li $v0, 10
    syscall

