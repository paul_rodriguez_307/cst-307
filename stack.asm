# stack.asm -- CLC
#
# Brandon Potter, LaMarr Pace, Paul Rodriguez
# Implements the Fibonacci sequence in a recursive
# function call in MIPS assembly.
# ================================================
	
main:
	# == Stack entry == 
	sub	$sp, $sp, 4	# Save $ra --
	sw	$ra, ($sp)	# and setup our frame
	la	$fp, ($sp)	# pointer.
	# =================

	
	# == Prompt and read ==
	li	$v0, 4		# Print our prompt
	la	$a0, Prompt	# ...
	syscall
	li	$v0, 5		# and read in a number.
	syscall
	# =====================


	# == Calculate Fibonacci number ==
	move	$a0, $v0	# Fib function expects
	jal	Fib		# its parameter in $a0.
	# ================================

	# == Print result ==
	move	$a0, $v0	# Fib stores its return
	li	$v0, 1		# number in $v0. Move it
	syscall			# to $a0 to be printed and
				# load the print int call.
	# ==================

	# == Stack exit ==
	la	$sp, ($fp)	# Restore the stack pointer.
	lw	$ra, ($sp)	# Restore $ra.
	add	$sp, $sp, 4	# Clean up stack.
	# ================
	
	jr	$ra		# Exit.

Fib:
	# The Fibonacci sequence is defined as:
	#	F(n) = F(n-1) + F(n-2)
	# with seeds F(2) = 1 and F(1) = 1 in our implentation.
	# Paramenter is passed in $a0, and the returned number
	# is passed back in $v0.

	# == Stack entry ==
	sub	$sp, $sp, 4	# Push $ra onto stack.
	sw	$ra, ($sp)
	sub	$sp, $sp, 4	# Push old frame pointer ($fp)
	sw	$fp, ($sp)	# onto stack.
	la	$fp, ($sp)	# Make $fp point to bottom of
	sub	$sp, $sp, 8	# stack frame. Make space for
	sw	$t0, -4($fp)	# 2 registers. Save $t0 and $a0.
	sw	$a0, -8($fp)
	# ==================

	# == Stop condition ==
	blt	$a0, 3, stop_recurse	# This is the stop
					# condition.
					# Fib(2) = 1, Fib(1) = 1
	# ====================

	# == 1st previous term ==
	sub	$a0, $a0, 1	# Calculate F(n-1)
	jal	Fib		# ...
	move	$t0, $v0	# Save the result.
	# =======================
	
	# == 2nd previous term ==
	lw	$a0, -8($fp)	# Because each function call
				# expects $a0 as its parameter
				# and manipulates it directly,
				# it has to be restored to
				# calculate the next number F(n-2).
	
	sub	$a0, $a0, 2	# Calculate F(n-2)
	jal	Fib		# ...
	# =======================

	# == Setup return value ==
	add	$v0, $t0, $v0	# Shortcut! Because $v0 has our 2nd
				# term and addition is communitive,
				# the 1st saved term in $t0 can just
				# be added to $v0 to save stack space
				# and instruction cycles.
	
	j	recurse		# Skip over the stop condition.
	# ========================
	
	# == Stop condition ==
stop_recurse:
	li	$v0, 1		# This for F(2) = F(1) = 1
	# ====================
	
recurse:			# In either case, recurse or stop,
				# the stack frame needs to be
				# dismantled and registers restored.

	# == Stack exit == 
	lw	$t0, -4($fp)	# Restore $t0. $a0 is ignored because
				# it's not need anymore.
	la	$sp, ($fp)	# Clean up stack by going back to the base.
	lw	$fp, ($sp)	# Restore our old frame pointer.
	add	$sp, $sp, 4	# ...
	lw	$ra, ($sp)	# Restore return variable.
	add	$sp, $sp, 4	# ...
	# =================
	
	jr	$ra		# and return.

	
# ==============================
.data	# String constants.
Prompt:	.asciiz "Please enter a number: "
	
